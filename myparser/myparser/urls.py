from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = [
    url(r'^ktweepy/', include('ktweepy.urls')),
    url(r'^admin/', include(admin.site.urls)),
]